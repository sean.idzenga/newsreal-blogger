// The json object will have a member called feed, so you'll pass feed.feed into this function
window.postBuilder = {

	build : function(feed){

		for (var i = 0; i < feed.length; i++){

			if(!this._isCategory('podcast',feed[i].category)){
				
				this._makeTile(feed[i]);
			}
		}
	},

	buildPage : function(feed){

		this._makePage(this._getPageByLink(feed));

	},

	buildDetail : function(feed){

		this._makeDetail(this._getPageByLink(feed));
	},

	// Used to render markup for each entry
	_makeTile : function(entry){

		var html = '';
		var main = document.getElementById('main');
		var posthref = this._getPostLink(entry.link);
		var div = document.createElement('div');
		var pubDate = this._formatDate(entry.published.$t);

		div.className = 'post-tile ';
		div.id = this._getPostId(entry.id.$t);

		if (this._isCategory('newsreal', entry.category)){

			div.className += 'newsreal';

			html += '<div class="post-header">' +
						'<h1 class="title">N3W5R34L</h1>' +
						'<div class="episode-date">' +
							'<span>' + entry.title.$t + '</span>' +
							'<span>' + pubDate + '</span>' +
						'</div>' +
					'</div>' +
					'<div class="post-content">' +
						'<div class="overlay"></div>' +
						entry.content.$t +
					'</div>' +
					'<div class="post-footer">' +
						'<a class="read-more" href="' + posthref + '">READ MORE</a>' +
					'</div>';
		} else {

			html += '<div class="post-content">' +
						entry.content.$t +
					'</div>';
		}

		div.innerHTML = html;
		main.appendChild(div);

	},

	_makePage : function(entry){

		var html = '';
		var main = document.getElementById('main');
		var div = document.createElement('div');

		div.className = 'post-tile';

		html += entry.content.$t;
		div.innerHTML = html;
		main.appendChild(div);

		window.pageEntry = entry;

	},

	_makeDetail : function(entry){

		var html = '';
		var main = document.getElementById('main');
		var div = document.createElement('div');
		var pubDate = this._formatDate(entry.published.$t);

		div.className = 'post-tile';

		html += '<div class="post-header">' +
					'<h1 class="title">N3W5R34L</h1>' +
					'<div class="episode-date">' +
						'<span>' + entry.title.$t + '</span>' +
						'<span>' + pubDate + '</span>' +
					'</div>' +
				'</div>' +
				'<div class="post-content">' +
					entry.content.$t +
				'</div>';

		div.innerHTML = html;
		main.appendChild(div);

	},

	_getPostId : function(inString){

		var rtrnString = inString.match(/(?:post-)(\d+)/i);
		return rtrnString[1];
	},

	_getPostLink : function(links){

		var rtrnString = '';
		
		for (var i = 0; i < links.length; i++){
			if(links[i].rel === 'alternate'){
				rtrnString = links[i].href;
				break;
			}
		}

		return rtrnString;
	},

	_getPageByLink : function(entries){

		// compare links in list of entries by window.location.pathname "/p/somepage.html"
		//
		var rtrnEntry = '';
		var matchString = window.location.pathname;

		for (var i = 0; i < entries.length; i++){

			var links = entries[i].link;
			for (var j = 0; j < links.length; j++){
				if (links[j].href.indexOf(matchString) > -1){
					rtrnEntry = entries[i];
					break;
				}
			}
		}

		return rtrnEntry;
	},

	_formatDate : function(inDate){

		var pubDate = new Date(inDate);
		var monthVal = pubDate.getMonth() + 1;
		var month = monthVal.toString();
		var date = pubDate.getDate().toString();
		var hour = pubDate.getHours().toString();
		var minute = pubDate.getMinutes().toString();

		if (month.length < 2){
			month = '0' + month;
		}

		if (date.length < 2){
			date = '0' + date;
		}

		if (hour.length < 2){
			hour = '0' + hour;
		}

		if (minute.length < 2){
			minute = '0' + minute;
		}

		return pubDate.getFullYear() + '-' + month + '-' + date + ' ' + hour + ':' + minute;

	},

	_isCategory : function(searchTerm, categories){

		var rtrnBool;

		if (typeof categories !== "undefined"){
			
			for (k = 0; k < categories.length; k++){
				if (categories[k].term === searchTerm){
					rtrnBool = true;
					break;
				}
			}
		} else {
			rtrnBool = false;
		}

		return rtrnBool;
	},

	_isHome : function(){
		return (window.location.pathname === '/');
	},

	_isPostDetail : function(){
		return (window.location.pathname).match(/\/[0-9]{4}\/[0-9]{2}\/\S+\.html/i) !== null;
	},

	_isPage : function(){
		return (window.location.pathname).match(/\/p\/\S+\.html/i) !== null;
	}

}