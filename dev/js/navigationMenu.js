window.navigationMenu = {

	_setActive : function(){

		if (typeof window.pageEntry !== 'undefined'){

			var navItemId = pageEntry.title.$t.replace(' ', '-').toLowerCase();
			$('.' + navItemId).addClass('active');

		}
	},

	_responsiveHide : function(){

		if (window.innerWidth > 802){

			$('nav').removeClass('hide');

		} else {

			$('nav').addClass('hide');
		}
	}

}