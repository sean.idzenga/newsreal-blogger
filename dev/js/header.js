window.sticky = function(){

	if ($(document).scrollTop() > 255) {

		addStick();		

	} else {

		removeStick();

	}
}

window.addStick = function(){

	$('header').addClass('sticky');
	$('nav').addClass('sticky');
	$('#main').addClass('sticky');
	$('.shadow').addClass('sticky');

}

window.removeStick = function(){

	$('header').removeClass('sticky');
	$('nav').removeClass('sticky');
	$('#main').removeClass('sticky');
	$('.shadow').removeClass('sticky');

}